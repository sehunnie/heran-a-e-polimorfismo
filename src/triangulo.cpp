#include "triangulo.hpp"
#include <iostream>
#include <math.h>

Triangulo::Triangulo(){
    set_tipo("Triangulo");
    set_base(10);
    set_altura(10);
}

Triangulo::Triangulo(float base,float altura){
    set_tipo("Triangulo");
    set_base(base);
    set_altura(altura);
}

float Triangulo::calcula_area(){
    return (get_altura()*get_base())/2;
}

float Triangulo::calcula_perimetro(){
    float lado=pow((get_base()/2),2) + pow(get_altura(),2);
    lado=sqrt(lado);
    return get_base()+2*lado;
}