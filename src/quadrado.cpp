#include "quadrado.hpp"
#include <iostream>

Quadrado::Quadrado(){
    set_tipo("Quadrado");
    set_base(10);
    set_altura(10);
}

Quadrado::Quadrado(float base){
    set_tipo("Quadrado");
    set_base(base);
    set_altura(base);
}

Quadrado::Quadrado(float base,float altura){
    set_tipo("Quadrado");
    set_base(base);
    set_altura(altura);
}