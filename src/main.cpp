#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "hexagono.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "triangulo.hpp"
#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char const *argv[]){
    FormaGeometrica *formagenerica = new FormaGeometrica(2,3);
    Triangulo *forma1 = new Triangulo(3.5, 4);
    Quadrado *forma2 = new Quadrado(12.0, 12.0);
    Circulo *forma3 = new Circulo(12);
    Hexagono *forma4 = new Hexagono(4,3);
    Pentagono *forma5 = new Pentagono(5,6);
    Paralelogramo *forma6 = new Paralelogramo(10,12,8);

    vector <FormaGeometrica*> lista;
    lista.push_back(formagenerica);
    lista.push_back(forma1);
    lista.push_back(forma2);
    lista.push_back(forma3);
    lista.push_back(forma4);
    lista.push_back(forma5);
    lista.push_back(forma6);


    for(int i=0; i<7; i++){
        cout << "Forma: "<< lista[i]->get_tipo() << endl;
        cout << "Área: "<< lista[i]->calcula_area() << endl;
        cout << "Perímetro: "<< lista[i]->calcula_perimetro() << endl<<endl;
    }

  return 0;
  
}
